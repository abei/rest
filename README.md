# rest

This is a small project for unifying the format of response of remote server invocation. Specially, If your service architecture is split-end mode
, the unification is more important.
The idea that I push this project comes from my own work experience, and here, I will give a demo as following, and everyone can extense the function of this project.
When you create new project to provide some services through interface(HTTP - json/application), you can import the jar to unify the format of response of the serivce
when each invocation by other remote service.

{
	"status": {
		"code": 0,
		"messag": "SUCC"
	},
	"data": "WORLD",
	"timestamp": 1554561297704
}