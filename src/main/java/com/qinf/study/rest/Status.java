package com.qinf.study.rest;

import lombok.Data;

/**
 * Created by qinf on 2019-04-06.
 */
@Data
public class Status {

    private int code;
    private String message;
    public static final Status SUCC = define(0, "SUCC");
    public static final Status FAIL = define(-1, "FAIL");
    public static final Status PARAM_INVALID = define(-2, "The param is invalid.");
    public static final Status DB_EXCEPTION = define(-4, "Database exception.");
    public static final Status REDIS_EXCEPTION = define(-5, "Redis exception.");
    public static final Status MQ_EXCEPTION = define(-6, "Message bus exception.");
    public static final Status RPC_EXCEPTION = define(-7, "Remote service invoke exception");
    public static final Status DATA_NOT_EXIST = define(-3, "The data not exist.");
    public static final Status UNKNOWN_EXCEPTION = define(-100, "Unknown exception.");

    private Status(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static Status define(int code, String message) {
        return new Status(code, message);
    }

    public static Status define(int code) {
        return define(code, null);
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
