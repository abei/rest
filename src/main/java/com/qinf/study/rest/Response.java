package com.qinf.study.rest;

import lombok.Data;

/**
 * Created by qinf on 2019-04-06.
 */
@Data
public class Response<T> {
    private Status status;
    private long timestamp;
    private T data;

    public Response() {
        this.timestamp = System.currentTimeMillis();
    }

    public static <T> Response<T> succ() {
        return succ(null);
    }

    public static <T> Response<T> succ(T data) {
        return succ(System.currentTimeMillis(), data);
    }

    public static <T> Response<T> succ(long timestamp, T data) {
        return of(Status.SUCC, data);
    }

    public static <T> Response<T> fail() {
        return of(Status.FAIL, null);
    }

    public static <T> Response<T> fail(Status status) {
        return of(status, null);
    }

    public static <T> Response<T> of(Status status, T data) {
        return of(status, System.currentTimeMillis(), data);
    }

    private static <T> Response<T> of(Status status, long timestamp, T data) {
        Response<T> res = new Response();
        res.setStatus(status);
        res.setTimestamp(timestamp);
        res.setData(data);
        return res;
    }
}